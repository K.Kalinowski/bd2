const express = require("express");
const router = express.Router()

const ProductsCtrl = require('../controllers/products-ctrl')

router.get('/products', ProductsCtrl.getProducts)
router.post('/products', ProductsCtrl.addProduct)
router.put('/products/:id', ProductsCtrl.updateProduct)
router.delete('/products/:id', ProductsCtrl.deleteProduct)
router.get('/products-raport', ProductsCtrl.getRaport)

module.exports = router;