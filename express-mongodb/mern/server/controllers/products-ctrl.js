const dbo = require("../db/conn");
const ObjectId = require("mongodb").ObjectId;

getProducts = async (req, res) => {
    const db_connect =  await dbo.getDb("store");
    let filter = {};
    if (req.query.name) {
      filter.name = req.query.name;
    }
    if (req.query.price) {
      filter.price = req.query.price;
    }
    if (req.query.quantity) {
      filter.quantity = req.query.quantity;
    }
    let sort = {};
    if (req.query.sortBy === "name") {
      sort.name = 1;
    } else if (req.query.sortBy === "price") {
      sort.price = 1;
    } else if (req.query.sortBy === "quantity") {
      sort.quantity = 1;
    }
    db_connect.collection("products").find(filter).sort(sort).toArray(function(err, result) {
        if (err) throw err;
        res.status(200).json(result);
    });
}

addProduct = (req, res) => {
    const db_connect = dbo.getDb("store");
    const myobj = {
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        quantity: req.body.quantity,
        measure: req.body.measure
    };
    db_connect.collection("products").findOne({name: req.body.name}, (err, product) => {
        if (err) throw err
        if (product) {
            res.status(400).json({msg: "Product already exists"})
        } else {
            db_connect.collection("products").insertOne(myobj, function(err, result){
                if (err) throw err
                res.status(200).json(result);
            });
        }
    })
    
}

updateProduct = (req, res) => {
    const db_connect = dbo.getDb("store");
    const myquery = {_id: ObjectId(req.params.id)};
    const newValues = {
        $set: {
            name: req.body.name,
            price: req.body.price,
            description: req.body.description,
            quantity: req.body.quantity,
            measure: req.body.measure
        },
    };
    db_connect.collection("products").updateOne(myquery, newValues, function(err, result){
        if (err) throw err
        console.log("1 document updated successfully");
        res.status(200).json(result);
    });
}

deleteProduct = (req, res) => {
    const db_connect = dbo.getDb("store");
    const myquery = {_id: ObjectId(req.params.id)};
    db_connect.collection("products").findOne(myquery, (err, product) => {
        if (err) throw err
        if (product) {
            db_connect.collection("products").deleteOne(myquery, function(err, result){
                if (err) throw err
                console.log("1 document deleted");
                res.status(200).json({});
            });
        } else {
            res.status(400).json({msg: "Product didn't exists"})
        }
    })
}

getRaport = (req, res) => {
    const db_connect = dbo.getDb("store");
    db_connect.collection("products").aggregate([{$group: {_id:"$name", totalQuantity:{$sum:"$quantity"}, totalValue:{$sum:{$multiply:["$price", "$quantity"]}}}}])
    .toArray((err, result) => {
            if (err) throw err;
            res.status(200).json({ report: result });
            client.close();
        });
}

module.exports = {
    getProducts,
    addProduct,
    updateProduct,
    deleteProduct,
    getRaport
};