from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os

load_dotenv()

app = Flask(__name__)

uri = os.getenv('URI')
user = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=(user, password),database="neo4j")

@app.route("/employees", methods=["GET"])
def get_employees():
  name = request.args.get("name")
  surname = request.args.get("surname")
  position = request.args.get("position")
  sort_by = request.args.get("sort_by")
  sort_order = request.args.get("sort_order")

  with driver.session() as session:
    result = session.run("""
      MATCH (e:Employee)
      WHERE ($name IS NULL OR e.name = $name) AND ($surname IS NULL OR e.surname = $surname) AND ($position IS NULL OR e.position = $position)
      RETURN e.name as name, e.surname as surname, e.position as position
      ORDER BY e.{} {}
    """.format(sort_by or "name", sort_order or "ASC"), name=name, surname=surname, position=position)
    
    return jsonify({'employees': [{"name": record["name"], "surname": record["surname"], "position": record["position"]} for record in result]})

@app.route("/employees", methods=["POST"])
def add_employee():
  name = request.json.get("name")
  surname = request.json.get("surname")
  position = request.json.get("position")
  department = request.json.get("department")
  
  if not name or not surname or not position or not department:
    return jsonify({"msg": "All fields are required"}), 400
  
  with driver.session() as session:
    result = session.run("MATCH (e:Employee) WHERE e.name = $name AND e.surname = $surname RETURN e.name as name, e.surname as surname", name=name, surname=surname)
    if result.single():
      return jsonify({"msg": "Employee already exists"}), 400

  with driver.session() as session:
    session.run("CREATE (e:Employee {name: $name,surname: $surname,position: $position,department: $department})", name=name, surname=surname, position=position, department=department)
  
  return jsonify({"msg": "Employee added"}), 201

@app.route("/employees/<id>", methods=["PUT"])
def edit_employee(id):
  name = request.json.get("name")
  surname = request.json.get("surname")
  position = request.json.get("position")
  department = request.json.get("department")
  
  with driver.session() as session:
    result = session.run("""
      MATCH (e:Employee) WHERE ID(e) = $id
      SET e.name = $name, e.surname = $surname, e.position = $position, e.department = $department
      RETURN e.name as name, e.surname as surname
    """, id=int(id), name=name, surname=surname, position=position, department=department)
    if not result.single():
      return jsonify({"msg": "Employee not found"}), 404
      
  return jsonify({"msg": "Employee updated"}), 200

@app.route("/employees/<id>", methods=["DELETE"])
def delete_employee(id):
  with driver.session() as session:
    result = session.run("MATCH (e:Employee)-[:MANAGES]->(d:Department) WHERE ID(e) = $id RETURN d", id=int(id))
    if result.single():
      session.run("""
        MATCH (e:Employee)-[:MANAGES]->(d:Department) WHERE ID(e) = $id
        MATCH (m:Employee) WHERE NOT (m)-[:MANAGES]->()
        SET d.manager = m DELETE e""", id=int(id))
    else:
      session.run("MATCH (e:Employee) WHERE ID(e) = $id DELETE e", id=int(id))
  
    return jsonify({"msg": "Employee deleted"}), 200

@app.route("/employees/<id>/subordinates", methods=["GET"])
def get_subordinates(id):
  with driver.session() as session:
    result = session.run("""
      MATCH (e:Employee)-[:MANAGES]->(d:Department)<-[:WORKS_IN]-(s:Employee)
      WHERE ID(e) = $id RETURN s""", id=int(id))
    
    subordinates = [{"id": record["s"].id, "name": record["s"]["name"], "position": record["s"]["position"], "department": record["d"]["name"]} for record in result]
    
    return jsonify({"subordinates": subordinates}), 200

@app.route('/employees/<id>/department', methods=['GET'])
def get_employee_department(id):
  with driver.session() as session:
    result = session.run("""
    MATCH (e:Employee)-[:WORKS_IN]->(d:Department) WHERE ID(e) = {employee_id}
    RETURN d.name AS name, (COUNT(*) - 1) AS employees_count, (MATCH (d)<-[:MANAGES]-(m:Employee) RETURN m)[0].name AS manager
    """, id=int(id))

    if not result:
        return jsonify({'msg': 'Employee not found'}), 404
    return jsonify(result[0]), 200

@app.route("/departments", methods=["GET"])
def get_departments():
  with driver.session() as session:
    departments = session.run("MATCH (d:Department) RETURN d").values()

    name = request.args.get("name")
    if name:
      departments = [d for d in departments if d["d"]["name"] == name]
    
    return jsonify([{"name": d["d"]["name"], "manager_name": d["d"]["manager_name"]} for d in departments])

@app.route('/departments/<id>/employees', methods=['GET'])
def get_employees_by_department(id):
  with driver.session() as session:
    employees = session.run("MATCH (d:Department)-[:WORKS_IN]->(e:Employee) WHERE ID(d) = $id RETURN e", id=int(id))

    return jsonify({"employees": employees}), 200

if __name__ == '__main__':
  app.run()
